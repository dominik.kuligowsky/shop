**This is my first big spring project to portfolio.
It is back-end of shop application. I wrote this project in java 11 using spring boot 2.**


I tried to create as good code as I can. I created it using SOLID and other good programming practice, and safety rules (like DTO-DAO).

Everything I code there is demonstration of my skills, I know that I didn't do everything and not implemented some features, there should be more 
code that handles exceptions, add some functionalities to user/product/basket/session etc,
but as I have written above, this project is only demonstration. Everything is still being updated by myself.
In the future I am planning to go full-stack and create online shop using improved version of this code.

**What you can find in my project:**

   
- REST APIs   
- CRUD for different objects (User, Product etc)
- Security for user : jwt - authentication, authorization, session managment
- Security practice for example DTO-DAO
- Auditing
- Validation - spring provided and custom validators
- Caching 
- Exception handling
- Exceptions (custom)
- Unit tests
- Project patterns like Factory or Strategy


Databases are based on mysql service.

**Technologies, frameworks and libraries which I use here:**
- Java 11
- Spring boot 2
- JPA 
- Spring Validator
- Spring Security
- Hazelcast
- Jwt
- Flyway
- Lombook
- Mapstruct
- Mysql
- Spock




