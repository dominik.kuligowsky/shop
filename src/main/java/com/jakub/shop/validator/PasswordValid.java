package com.jakub.shop.validator;

import com.jakub.shop.validator.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface PasswordValid{

    String message() default "Password and confirmed password are not same";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
