package com.jakub.shop.generic.strategy.mail;

import com.jakub.shop.generic.strategy.GenericStrategy;
import com.jakub.shop.generic.strategy.mail.model.MailType;

public interface MailStrategy extends GenericStrategy<MailType> {
    void sendMail();
}
