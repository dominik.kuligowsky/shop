package com.jakub.shop.generic.strategy.mail.model;

public enum MailType {
    REGISTER,
    RESTART_PASSWORD
}
