package com.jakub.shop.generic.strategy.file.impl;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generic.strategy.file.FileGeneratorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CsvFileGeneratorStrategyImpl implements FileGeneratorStrategy {
    @Override
    public FileType getType() {
        return FileType.CSV;
    }

    @Override
    public byte[] generateFile() {
        log.info("Csv");
        return new byte[0];
    }
}
