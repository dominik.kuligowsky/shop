package com.jakub.shop.generic.strategy;

public interface GenericStrategy <T> {
    T getType();
}
