package com.jakub.shop.generator.strategy.impl;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generator.strategy.GeneratorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CsvGeneratorStrategyImpl implements GeneratorStrategy {
    @Override
    public FileType getType() {
        return FileType.CSV;
    }

    @Override
    public byte[] generateFile() {
        log.info("CSV");
        return new byte[0];
    }
}
