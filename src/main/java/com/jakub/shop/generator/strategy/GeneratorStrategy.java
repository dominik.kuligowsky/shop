package com.jakub.shop.generator.strategy;

import com.jakub.shop.generator.model.FileType;

public interface GeneratorStrategy {
    FileType getType();
    byte[] generateFile();
}
