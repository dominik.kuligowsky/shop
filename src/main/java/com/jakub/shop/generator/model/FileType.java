package com.jakub.shop.generator.model;

public enum FileType {
    PDF,
    XLS,
    CSV
}
