package com.jakub.shop.mapper.history;

import com.jakub.shop.domain.dao.Product;
import com.jakub.shop.domain.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.history.Revision;

@Mapper(componentModel = "spring")
public interface ProductHistoryMapper {
    @Mapping(source = "entity.id", target = "id")
    @Mapping(source = "entity.name", target = "name")
    @Mapping(source = "entity.price", target = "price")
    @Mapping(source = "entity.quantity", target = "quantity")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")
    ProductDto toDto(Revision<Integer, Product> revision);
}
