package com.jakub.shop.mapper;

import com.jakub.shop.domain.dao.Product;
import com.jakub.shop.domain.dto.ProductDto;

import java.util.List;

public interface ProductMapper {
    Product toDao(ProductDto productDto);
    ProductDto toDto(Product product);
    List<ProductDto> toListDto(List<Product> products);
}
