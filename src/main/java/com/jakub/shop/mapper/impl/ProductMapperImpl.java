package com.jakub.shop.mapper.impl;

import com.jakub.shop.domain.dao.Product;
import com.jakub.shop.domain.dto.ProductDto;
import com.jakub.shop.mapper.ProductMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public Product toDao(ProductDto productDto) {
        return Product.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .price(productDto.getPrice())
                .quantity(productDto.getQuantity())
                .build();
    }

    @Override
    public ProductDto toDto(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
    }

    @Override
    public List<ProductDto> toListDto(List<Product> products) {
        return products.stream().map(this::toDto).collect(Collectors.toList());
    }


}
