package com.jakub.shop.service;

import com.jakub.shop.domain.dao.Basket;
import com.jakub.shop.domain.dao.Product;

import java.util.List;

public interface BasketService {
    Basket addToBasket(Product product);
    void removeProductFromBasket(Long productId);
    Basket updateProductQuantityInBasket(Product product);
    List<Product> getBasket();


}
