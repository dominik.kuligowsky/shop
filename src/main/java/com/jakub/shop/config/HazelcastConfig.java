package com.jakub.shop.config;

import com.hazelcast.config.*;
import com.jakub.shop.domain.dao.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfig {
    @Bean
    Config configHazelcast() {
        Config config = new Config()

                .setInstanceName("instance-1")

                .addMapConfig(new MapConfig()
                        .setName("product")
                        .setEvictionConfig(new EvictionConfig()
                                .setEvictionPolicy(EvictionPolicy.LRU)
                                .setSize(10)
                                .setMaxSizePolicy(MaxSizePolicy.FREE_HEAP_SIZE))
                        .setTimeToLiveSeconds(60 * 60 * 24));

        config.getSerializationConfig().addDataSerializableFactory
                (1, (int id) -> (id == 1) ? new Product() : null);

        return config;
    }



}
