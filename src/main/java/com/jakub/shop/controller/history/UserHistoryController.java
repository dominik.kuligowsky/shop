package com.jakub.shop.controller.history;

import com.jakub.shop.domain.dto.UserDto;
import com.jakub.shop.mapper.history.UserHistoryMapper;
import com.jakub.shop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/history/users")
@RequiredArgsConstructor
public class UserHistoryController {
    private final UserRepository userRepository;
    private final UserHistoryMapper userHistoryMapper;

    @GetMapping("/{id}")
    public Page<UserDto> getUserHistory(@RequestParam int page, @RequestParam int size, @PathVariable Long id){
        return userRepository.findRevisions(id, PageRequest.of(page, size)).map(userHistoryMapper::toDto);
    }
}
