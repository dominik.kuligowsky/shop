package com.jakub.shop.controller;

import com.jakub.shop.domain.dto.ProductDto;
import com.jakub.shop.mapper.ProductMapper;
import com.jakub.shop.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/baskets")
@RequiredArgsConstructor
public class BasketController {
    private final BasketService basketService;
    private final ProductMapper productMapper;

    @PostMapping
    public void addBasket(@RequestBody @Valid ProductDto productDto){
        basketService.addToBasket(productMapper.toDao(productDto));
    }

    @PutMapping
    public void updateBasket(@RequestBody @Valid ProductDto productDto){
        basketService.updateProductQuantityInBasket(productMapper.toDao(productDto));
    }

    @DeleteMapping("/{productId}")
    public void deleteBasketById(@PathVariable Long productId){
        basketService.removeProductFromBasket(productId);
    }

    @GetMapping
    public List<ProductDto> getBasket(){
        return productMapper.toListDto(basketService.getBasket());
    }

}
