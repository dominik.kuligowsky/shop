package com.jakub.shop.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jakub.shop.validator.PasswordValid;
import com.jakub.shop.validator.group.UserCreate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@PasswordValid(groups = UserCreate.class)
public class UserDto {
    private Long id;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    @Email
    private String email;
    private String password;
    private String confirmPassword;
    private Integer revisionNumber;
}
