package com.jakub.shop.mapper

import com.jakub.shop.domain.dao.User
import com.jakub.shop.domain.dto.UserDto
import spock.lang.Specification

class UserMapperSpec extends Specification {
    def userMapper = new UserMapperImpl()

    def 'should return userDto'() {
        given:
        def user = User.builder()
                .id(1L)
                .firstName('firstname')
                .lastName('lastname')
                .email('mail@mail')
                .password('password')
                .build()

        when:
        def result = userMapper.toDto(user)

        then:
        result.id == 1L
        result.firstName == 'firstname'
        result.lastName == 'lastname'
        result.email == 'mail@mail'
        result.password == null

    }

    def 'toDto should return null when user is null'() {
        when:
        def result = userMapper.toDto(null)

        then:
        result == null
    }

    def 'should return user'() {
        given:
        def user = UserDto.builder()
                .id(1L)
                .firstName('firstname')
                .lastName('lastname')
                .email('mail@mail')
                .password("password")
                .build()

        when:
        def result = userMapper.toDao(user)

        then:
        result.id == 1L
        result.firstName == 'firstname'
        result.lastName == 'lastname'
        result.email == 'mail@mail'
        result.password == 'password'
    }

    def 'toDao should return null when user is null'() {
        when:
        def result = userMapper.toDao(null)

        then:
        result == null
    }

}