package com.jakub.shop.mapper

import com.jakub.shop.domain.dao.Product
import com.jakub.shop.domain.dto.ProductDto
import com.jakub.shop.mapper.impl.ProductMapperImpl
import spock.lang.Specification

class ProductMapperSpec extends Specification {
    def productMapper = new ProductMapperImpl()

    def 'should return ProductDto'(){
        given:
        def product = Product.builder()
                .id(1L)
                .name('TestProduct')
                .quantity(Double.valueOf(60.0))
                .price(Double.valueOf(10.0))
                .build()

        when:
        def result = productMapper.toDto(product)

        then:
        result.id == product.id
        result.name == product.name
        result.price == product.price
        result.quantity == product.quantity
    }

    def 'should throw NullPointerException when product is null'(){
        when:
        productMapper.toDto(null)

        then:
        thrown NullPointerException
    }

    def 'should return productDto'(){
        given:
        def product = ProductDto.builder()
                .id(1L)
                .name('TestProduct')
                .quantity(Double.valueOf(60.0))
                .price(Double.valueOf(10.0))
                .build()

        when:
        def result = productMapper.toDao(product)

        then:
        result.id == product.id
        result.name == product.name
        result.price == product.price
        result.quantity == product.quantity
    }

    def 'should throw NullPointerException when productDto is null'(){
        when:
        productMapper.toDao(null)

        then:
        thrown NullPointerException
    }
}
