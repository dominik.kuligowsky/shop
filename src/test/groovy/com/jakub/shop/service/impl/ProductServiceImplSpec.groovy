package com.jakub.shop.service.impl

import com.jakub.shop.domain.dao.Product
import com.jakub.shop.repository.ProductRepository
import spock.lang.Specification

import javax.persistence.EntityNotFoundException

class ProductServiceImplSpec extends Specification{
    def productService
    def productRepository = Mock(ProductRepository)

    def setup(){
        productService = new ProductServiceImpl(productRepository)
    }

    def 'should get product by id'(){
        when:
        productService.findById(1L)

        then:
        1 * productRepository.findById(1L) >> Optional.of(new Product())
        0 * _
    }

    def 'should throw exception when product does not exist'(){
        given:
        productRepository.findById(1L) >> Optional.empty()

        when:
        productService.findById(1L)

        then:
        thrown EntityNotFoundException
    }

    def 'should save product'(){
        given:
        def product = new Product()

        when:
        productService.save(product)

        then:
        1 * productRepository.save(product)
        0 * _
    }

    def 'should delete product'(){
        when:
        productService.deleteById(1L)

        then:
        1 * productRepository.deleteById(1L)
        0 * _
    }

}
